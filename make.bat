@rem script to build an e3 version for WinDOS
@rem 16 bit DOS .COM version
NASMW -DCOM e3-16.asm  -l e3com16.lst -o e3-16.com  -f bin
@rem
@rem 16 bit DOS .EXE version
@rem NASMW -DEXE e3-16.asm  -l e3exe16.lst -o e3-16e.exe -f bin
@rem
@rem 16 bit DOS EXE-STUB version for e3.exe
NASMW -DEXESTUB e3-16.asm  -l e3stub.lst -o e3stub.obj -f obj
ALINK e3stub.obj
@rem
@rem 32 bit e3.exe itself
NASMW -DW32 e3.asm  -l e3.lst               -f win32
@rem
@rem link all togehther
ALINK -oPE -subsys console e3 win32.lib -entry _start -stub e3stub.exe
@rem
@rem install into path
copy e3.exe C:\windows\e3.exe
